-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2018 at 02:39 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gigfit`
--

-- --------------------------------------------------------

--
-- Table structure for table `bpdb`
--

CREATE TABLE `bpdb` (
  `id_admin` int(10) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bpdb`
--

INSERT INTO `bpdb` (`id_admin`, `username`, `email`, `password`) VALUES
(17523126, 'ihyaet', 'ihyaet@gigfit.co.id', '151198ihya'),
(17523127, 'aaarif', 'marifidris@gigifit.co.id', 'ariff456'),
(17523187, 'fadell', 'fadelmuhammad@gigifit.co.id', 'fadel1234');

-- --------------------------------------------------------

--
-- Table structure for table `laporan`
--

CREATE TABLE `laporan` (
  `id_laporan` int(5) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `pelapor` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `tanggal` date NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `media` varchar(100) NOT NULL,
  `level_kerusakan` varchar(10) DEFAULT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laporan`
--

INSERT INTO `laporan` (`id_laporan`, `judul`, `pelapor`, `deskripsi`, `tanggal`, `alamat`, `media`, `level_kerusakan`, `status`) VALUES
(23, 'Jalan Hancur Gara-Gara Time Majin', 'Tokiwa Sougo', 'Maaf pak dinas saya diajakin Time Jacker tarung pake Time Majin ya sudah saya kombo, tapi jalannya kena juga', '2018-04-04', 'Distrik Minagawa', '', 'Sedang', 'Diterima'),
(25, 'Jalan berlubang', 'Myokoin Geiz', 'Lubang di jalan ini sangat banyak, mohon segera diperbaiki', '2018-12-13', 'Nun jauh disana saya lihat', '', 'Sedang', 'Diterima'),
(26, 'Lampu Penerangan di daerah semanggi', 'Tsukuyomi', 'Lampunya mati mungkin karena terkena hujan berhari basah hingga dalam dan mati, mohon pada dinas setempat segera memperbaikinya karena daerah tersebut butuh penerangan', '2018-12-13', 'Semanggi , Jakarta ', '118676.jpg', 'Sedang', 'Diterima'),
(27, 'hati saya rusak', 'King of Lust', 'jomblo 18 tahun', '2018-12-18', 'jalan sama dia tapi ga jadian', '20171022_061210.jpg', 'Berat', 'Ditolak'),
(29, 'Jalan Ada Monster', 'Tokiwa Sougo', 'Tolong TOlong', '2018-12-11', 'mana saja', '1522306_498135576970269_600330617_n.jpg', 'Ringan', 'Ditolak'),
(30, 'Mesjid Al Mutawatir Rusak', 'Tokiwa Sougo', 'Kerusakan akibat gempa beberapa waktu lalu\r\n', '2018-12-12', 'Kudus,', '', 'Berat', 'Diterima'),
(31, 'Jalan jalan ke si borong2', 'Myokoin Geiz', 'asdkjoajd oaijsdi akdjnwa', '2018-12-10', 'akjdwwdjiqomqiw qiwje', '7', NULL, ''),
(37, 'amsdnjskadn j', 'Myokoin Geiz', 'asdnksjd', '2016-11-29', 'nsakjdn', '', 'Pilih', 'Ditolak'),
(38, 'Telepon Umum di Halte', 'ibnu', 'kebakaran hutan jakal', '2018-12-14', 'Jl Kaliurang', '', 'Sedang', 'Diterima');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `no_telp`, `email`, `password`) VALUES
(17236202, 'Myokoin Geiz', '0826868686', 'geiz@mail.com', '22fe4b8ca680574ebd190b727762343a'),
(17236203, 'Woz Iwae', '1010101010', 'woz@mail.com', 'e42fa2acefadf7f88a288eb60278cb1d'),
(17236204, 'Tokiwa Sougo', '0820202020', 'jio@mail.com', '8b57b3f7736e4016c2f59ef3bbe05746'),
(17236206, 'Tsukuyomi', '0823981238', 'tsukuyomi@mail.com', '2f593dd78aae0e97cf127acee1a36199'),
(17236207, 'Fikri', '089999999999', 'wew@gmail.com', '4004fd57ebd6e3f3e76b25676673ab2c'),
(17236208, 'King of Lust', '085533213436756', 'asede@gmail.com', '6e03f246cc3f423d4dc2def48c6a85f2'),
(17236209, 'ibnu', '081231724124', 'ibnu@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bpdb`
--
ALTER TABLE `bpdb`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `laporan`
--
ALTER TABLE `laporan`
  ADD PRIMARY KEY (`id_laporan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `laporan`
--
ALTER TABLE `laporan`
  MODIFY `id_laporan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17236210;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

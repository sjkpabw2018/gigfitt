<?php
class Dinas extends CI_Controller{

	function __construct(){
		parent:: __construct();
		$this->load->model('M_laporan');
		$this->load->helper('url');
	}

	function index(){
		$data['laporan'] = $this->M_laporan->tampil_data()->result();
	    $this->load->view('distribusi', $data);
	}
}